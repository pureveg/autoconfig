#!/bin/bash
# Set up env in new server Ubuntu 12.04
# use under root


# generate ssh key
function genSSHKey(){
	ssh-keygen -t rsa -C "pureveg@163.com"
	echo ~/.ssh/id_rsa.pub
}

# get vimrc
function getVimrc(){
	git clone git@bitbucket.org:pureveg/vimrc.git
	mv ~/vimrc/.vimrc ~/.vimrc 
	rm -rf /vimrc
}

# Gen Locale
function genLocale(){
	echo 'export LANG="en_US.UTF-8"' >> /etc/profile
	echo 'export LC_ALL=$LANG' >> /etc/profile
	locale-gen en_US en_US.UTF-8
	dpkg-reconfigure locales
}

# Timezone set to UTC
function setTZ(){
	dpkg-reconfigure tzdata
}

# Build dependencies
function dependency(){
	apt-get update
	apt-get install -y gcc libc6-dev
	apt-get install -y mercurial
	apt-get install -y git
}

# Tmux
function setTMUX(){
	apt-get install -y tmux
	rm -rf ~/.tmux.conf
	git clone git@bitbucket.org:pureveg/tmux.git
	mv ~/tmux/.tmux.conf ~/.tmux.conf
	rm -rf ~/tmux
}

# install & setup go
function setGoEnv(){
	# Clone source code & build
	hg clone -u release https://code.google.com/p/go
	cd go/src && ./all.bash

	# Move go Root dir to / & chmod to 777
	rm -rf /go
	mv go /
	chmod 777 /go
	
	# Make GOPATH dir
	rm -rf /GoProject
	mkdir /GoProject
	chmod 777 /GoProject

	echo 'alias goinstall="clear && go install && go build"' >> /etc/profile
	echo 'alias gotest="clear && go test"' >> /etc/profile

	# Golang env variable
	echo 'export GOROOT=/go' >> /etc/profile
	echo 'export GOBIN=/go/bin' >> /etc/profile
	echo 'export GOPATH=/GoProject' >> /etc/profile
	echo 'export PATH=$PATH:$GOBIN:$GOPATH' >> /etc/profile
}

# Some alias & UI & Lang setting ( not sure if Lang is necessary )
function setAlias(){
	echo 'export PS1="\[\033[0;32m\][Pureveg]\[\033[00m\] @ \e[1;31m\w\[\033[00m\] \n\# $  "' >> ~/.bashrc
	echo 'export CLICOLOR=1' >> ~/.bashrc
	echo 'export GREP_OPTIONS="--color=auto"' >> ~/.bashrc
}

# initFuncialize the env
function initFunc(){
	(genLocale)
	(setTZ)
	(dependency)
	(setGoEnv)
	(setAlias)
	(getVimrc)
	(setTMUX)
}
echo "Do you want to initFuncialize the env"
echo "1. Yes"
read num
case "$num" in
	[1] ) (initFunc);;
	*) exit;;
esac

echo 'Add the ssh key to bitbucket and reboot the system...'
