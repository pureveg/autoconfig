#!/bin/bash

# Install some go package
go get -u github.com/astaxie/beego;
go get -u github.com/beego/bee;
go get -u github.com/lunny/xorm;
go get -u github.com/lib/pq;
go get -u github.com/nsf/gocode;
go get -u code.google.com/p/rsc/qr;
go get -u bitbucket.org/pureveg/gg2fa;
go get -u github.com/conformal/btcjson;
go get -u bitbucket.org/pureveg/wallet;
go get -u bitbucket.org/pureveg/utils;
go get -u github.com/jstemmer/gotags;

# Update VIM to 7.4
apt-get update;
apt-get install -y python-software-properties;
apt-add-repository -y ppa:fcwu-tw/ppa;
apt-get update;
apt-get install -y vim;

# Set up Vundle for VIM 
git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle;

# Install Configured Bundles ( only one, YCM )
vim +BundleInstall +qall;

# Compile YCM
apt-get install -y build-essential cmake;
apt-get install -y python-dev;
cd ~/.vim/bundle/YouCompleteMe && ./install.sh;

# YCM for golang
cp -r $GOROOT/misc/vim/* ~/.vim/;
cd $GOPATH/src/github.com/nsf/gocode/vim && ./update.sh;
gocode set propose-builtins true;

# install ctags
tar -xzvf ctags-5.8.tar.gz;
cd ctags-5.8 && ./configure && make && make install;
